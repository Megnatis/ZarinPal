package mehritco.ir.zarinpal;
/**
 * FarshiD Naqizadeh
 * Email : Mehr.db@gmail.com
 * Phone : 09179606277
 * Gitlab : Megnatis
 * @author Megnatis
 */
public  final class Param {
    /*
    For request
    برای ارسال درخواست 
    */
    public static final String PARAM_MERCHANT_ID = "MerchantID";
    public static final String PARAM_AMOUNT = "Amount";
    public static final String PARAM_DESCRIPTION = "Description";
    public static final String PARAM_EMAIL = "Email";
    public static final String PARAM_MOBILE = "Mobile";
    public static final String PARAM_CALLBACKURL = "CallbackURL";
    public static final String PARAM_ADDITIONALDATA = "AdditionalData";
    /*
    For response
    برای اخذ واکنش
    */
    public static final String PARAM_STATUS = "Status";
    public static final String PARAM_AUTHORITY = "Authority";
    public static final String PARAM_REF_ID = "RefID";
    public static final String PARAM_EXTRA_DETAIL = "ExtraDetail";

    /*
    rest api Json
    */
    public static final String PARAM_USER_AGENT = "ZarinPal Rest Api v1";
    public static final String PARAM_CONTENT_TYPE = "application/json";
    public static final String PARAM_URL_TO_GET_AUTHORITY = "https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json";
    public static final String PARAM_URL_TO_PAYMENT = "https://www.zarinpal.com/pg/StartPay/";
    public static final String PARAM_URL_TO_VERIFICATION = "https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json";
    /**
     * Manual : https://www.zarinpal.com/blog/%D8%B2%D8%B1%DB%8C%D9%86-%DA%AF%DB%8C%D8%AA%D8%8C-%D8%AF%D8%B1%DA%AF%D8%A7%D9%87%DB%8C-%D8%A7%D8%AE%D8%AA%D8%B5%D8%A7%D8%B5%DB%8C-%D8%A8%D9%87-%D9%86%D8%A7%D9%85-%D9%88%D8%A8%D8%B3%D8%A7%DB%8C%D8%AA/
     * برای ارجاع کاربر به درگاه پرشین سوئیچ برای ارجاع کاربر به درگاه سامان
     * برای ارجاع کاربر به درگاه سداد برای ارجاع کاربر به درگاه پارسیان برای
     * برای ارجاع کاربر به درگاه سداد
     * برای ارجاع کاربر به درگاه پارسیان
     * برای ارجاع کاربر به درگاه فناواکارت
     * ارجاع کاربر به درگاه فناواکارت برای ارجاع کاربر به درگاه زرین گیت
     * ارجاع به درگاه موبایل
     */
    public static final String PARAM_URL_TO_GETWAY[] = {"NON", "Asan", "Sep", "Sad", "Pec", "Fan", "ZarinGat","MobileGate","USSD"};
    
}
