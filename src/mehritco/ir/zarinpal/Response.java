package mehritco.ir.zarinpal;

import java.io.Serializable;

/**
 * FarshiD Naqizadeh
 * Email : Mehr.db@gmail.com
 * Phone : 09179606277
 * Gitlab : Megnatis
 * @author Megnatis
 */
public class Response implements Serializable{
    
    int responseCode;
    /**
     * status code manual page 14 @
     * https://codeload.github.com/ZarinPal-Lab/Documentation-PaymentGateway/zip/master
     */
    int status;
    String authority;
    Long minimalAuthority;
    Long refId;
    String ExtraDetail ;
    /**
     * Need to be set authority without authority this object can't
     * be done a job!
     */
    public Response(){}

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Long getMinimalAuthority() {
        return minimalAuthority;
    }

    public void setMinimalAuthority(Long minimalAuthority) {
        this.minimalAuthority = minimalAuthority;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getExtraDetail() {
        return ExtraDetail;
    }

    public void setExtraDetail(String ExtraDetail) {
        this.ExtraDetail = ExtraDetail;
    }
}
