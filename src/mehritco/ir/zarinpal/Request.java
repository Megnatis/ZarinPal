package mehritco.ir.zarinpal;

import java.io.Serializable;
import org.json.JSONObject;

/**
 * FarshiD Naqizadeh
 * Email : Mehr.db@gmail.com
 * Phone : 09179606277
 * Gitlab : Megnatis
 * @author Megnatis
 */
public class Request implements Serializable {
    /**
     * Like => "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"; Required
     */
    String merchantID;
    /**
     * Like => 10000 (Toman) ; Required
     */
    int amount;
    /**
     * Required
     */
    String description;
    /**
     * Optional
     */
    String email;
    /**
     * Optional
     */
    String mobile ;
    /**
     *  'http://www.yoursoteaddress.ir/verify.php'; // Required
     */
    String callbackURL;
    /**
     * Add json object for sending additional data // Optional
     */
    String AdditionalData;

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCallbackURL() {
        return callbackURL;
    }

    public void setCallbackURL(String callbackURL) {
        this.callbackURL = callbackURL;
    }

    public String getAdditionalData() {
        return AdditionalData;
    }

    public void setAdditionalData(String AdditionalData) {
        this.AdditionalData = AdditionalData;
    }
    
    
    
    
}
