package mehritco.ir.zarinpal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * FarshiD Naqizadeh
 *
 * @Email : Mehr.db@gmail.com Phone : 09179606277 Gitlab : Megnatis
 * @author Megnatis Remember Its Object Of Payment And all method in manual from
 * this link started by payment its here:
 * https://codeload.github.com/ZarinPal-Lab/Documentation-PaymentGateway/zip/master
 */
public class Payment {
    private int responseCode = 0;
    
    private String connect(String url, JSONObject data) {
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = null;
            con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", Param.PARAM_USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", Param.PARAM_CONTENT_TYPE);
            con.setRequestProperty("Accept-Charset", "UTF-8");
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(data.toString().getBytes("UTF-8"));
                wr.flush();
            }
            responseCode = con.getResponseCode();
            StringBuilder response;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
            }
            return response.toString();
        } catch (IOException ex) {
            System.out.println("ResponseCode : "+responseCode);
            System.out.println("Ex :" + ex.getMessage());
            return null;
        }
    }
    /**
     * To get responseCode
     * if it's 200 its ok , 404 its trouble !
     * @return 
     */
    public int getResponseCode() {
        return responseCode;
    }
    
    private JSONObject convertRequestToJson(Request request){
        JSONObject json = new JSONObject();
        json.put(Param.PARAM_MERCHANT_ID, request.getMerchantID());
        json.put(Param.PARAM_AMOUNT, request.getAmount());
        json.put(Param.PARAM_DESCRIPTION, request.getDescription());
        json.put(Param.PARAM_CALLBACKURL, request.getCallbackURL());
        json.put(Param.PARAM_EMAIL, request.getEmail());
        json.put(Param.PARAM_MOBILE, request.getMobile());
        json.put(Param.PARAM_ADDITIONALDATA, request.getAdditionalData());        
        return json;
    }
    private JSONObject convertResponseToJson(Response response){
    JSONObject json = new JSONObject();
    json.put(Param.PARAM_AUTHORITY, response.getAuthority());
    json.put(Param.PARAM_REF_ID, response.getRefId());
    json.put(Param.PARAM_STATUS, response.getStatus());
    return json;
    }
    /**
     * make a request to get authority
     * @param request need all required !
     * @return Response Object have STATUS and AUTHORITY
     */
    public Response request(Request request) {
        String back = connect(Param.PARAM_URL_TO_GET_AUTHORITY, convertRequestToJson(request));
        try{
            JSONObject json = new JSONObject(back);
            Response respnse = new Response();
            respnse.setAuthority(json.optString(Param.PARAM_AUTHORITY));
            respnse.setStatus(json.optInt(Param.PARAM_STATUS));
            return respnse;
        }catch(JSONException ex){
            System.out.println("Error :"+ex.getMessage());
            return null;
        }
    }
    /**
     * use this method to get authority url to open gate
     * for user!
     * @param gateType check your gate!
     * @param authority
     * @return URL to open gate for make a pay complete 
     */
    public String gate(int gateType , String authority){
    String urlPayment = Param.PARAM_URL_TO_PAYMENT;
        switch (gateType) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return urlPayment + authority + "/" + Param.PARAM_URL_TO_GETWAY[gateType];
            case 8:
                return "*770*97*2*" + Long.valueOf(authority) + "#";
            case 0:
            default:
                return urlPayment + authority;
        }
    }
    /**
     * this method use to get
     * @param request need merchentId And amount 
     * @param response need authority
     * @return 
     */
    public Response verification(Request request , Response response){
        JSONObject json = new JSONObject((convertRequestToJson(request).toString() + convertResponseToJson(response).toString()).replace("}{", ","));
        JSONObject jsonBack = new JSONObject(connect(Param.PARAM_URL_TO_VERIFICATION, json));
        Response responseBack = new Response();
        responseBack.setStatus(jsonBack.optInt(Param.PARAM_STATUS));
        responseBack.setRefId(jsonBack.optLong(Param.PARAM_REF_ID));
        responseBack.setExtraDetail(jsonBack.optString(Param.PARAM_EXTRA_DETAIL));
    return responseBack;
    }
}
