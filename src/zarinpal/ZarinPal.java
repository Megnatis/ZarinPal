/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zarinpal;

import mehritco.ir.zarinpal.Payment;
import mehritco.ir.zarinpal.Request;
import mehritco.ir.zarinpal.Response;
import org.json.JSONObject;

/**
 *
 * @author Megnatis
 */
public class ZarinPal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Request request = new Request();
        request.setMerchantID("XXXX-XXXX-XXXX-XXXX-XXXXXX");
        request.setAmount(1000);
        request.setDescription("سلام");
        request.setCallbackURL("http://mehritco.ir/url/get.jsp");
        request.setEmail("Ceo@mehritco.ir");
        request.setMobile("09179606277");
        JSONObject jsonAdd = new JSONObject();
        jsonAdd.put("expireIn", "18000");
        request.setAdditionalData(jsonAdd.toString());
        Payment pay = new Payment();
        Response response = pay.request(request);
        System.out.println(response.getAuthority());
        System.out.println("Gate : "+pay.gate(0, response.getAuthority()));
        Response x = new Response();
        x.setAuthority("0000000000000000000000000000XXXXXXXX");
        Response r = pay.verification(request, x);
        System.out.println("R : "+r.getRefId());
        System.out.println("add : "+r.getExtraDetail());
    }
    
}
